**résumé cahier des charges**

1) Mettre en place un FTP de dépôt ​ (structure de dossier à prévoir par bâtiment/étage)

2) Générer un fichier de données cible dans un dossier ​ sur ce même FTP après le traitement de
données nécessaires avant exploitation (mise au format, agrégation des fichiers...)

3) Mettre en place un tableau de bord ​ permettant de visualiser un certain nombre de métriques,
selon les critères de filtres suivants :
- par bâtiment / étage
- sur une période donnée (date/heure de début – date/heure de fin)

Cet outil pourra être installé sur une machine en local ou déployé sur un serveur et accessible via une page
de backOffice avec une sécurité d’accès (login/mot de passe).
Les métriques à mettre en évidence selon la Datavisualisation de votre choix, sont les suivantes :
Evolution de la température heure/heure

Mise en évidence de la température max et min, le jour et et la nuit sur la période observée

Mise en évidence de la température moyenne le jour et la nuit, calculée sur la période observée

Calcul du taux d’inconfort : correspond au nombre d’heures au-dessus de 28, 29, 30, 31, sur la
période observée et sur l’année.
