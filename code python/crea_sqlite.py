import csv, sqlite3

con = sqlite3.connect ('Etic.db')
cur = con.cursor()
#UCPA_111 creation et rmplissage de la table
cur.execute("CREATE TABLE if not exists UCPA_111 ('UCPA bureau 111','Time','Celsius(°C)','Serial Number');") 

with open('2005_UCPA_bureau_111.csv','r') as fin: 
    dr = csv.DictReader(fin) 
    to_db = [(i['UCPA bureau 111'], i['Time'],i['Celsius(°C)'], i['Serial Number'] ) for i in dr]

cur.executemany("INSERT INTO UCPA_111 ('UCPA bureau 111','Time','Celsius(°C)','Serial Number') VALUES (?, ?, ?, ?);", to_db)

with open('2006_bureau_111.csv','r') as fin: 
    dr = csv.DictReader(fin) 
    to_db = [(i['UCPA bureau 111'], i['Time'],i['Celsius(°C)'], i['Serial Number'] ) for i in dr]

cur.executemany("INSERT INTO UCPA_111 ('UCPA bureau 111','Time','Celsius(°C)','Serial Number') VALUES (?, ?, ?, ?);", to_db)

with open('2007_111_UCPA.csv','r') as fin: 
    dr = csv.DictReader(fin) 
    to_db = [(i['UCPA bureau 111'], i['Time'],i['Celsius(°C)'], i['Serial Number'] ) for i in dr]

cur.executemany("INSERT INTO UCPA_111 ('UCPA bureau 111','Time','Celsius(°C)','Serial Number') VALUES (?, ?, ?, ?);", to_db)

with open('2008_111_UCPA.csv','r') as fin: 
    dr = csv.DictReader(fin) 
    to_db = [(i['UCPA bureau 111'], i['Time'],i['Celsius(°C)'], i['Serial Number'] ) for i in dr]

cur.executemany("INSERT INTO UCPA_111 ('UCPA bureau 111','Time','Celsius(°C)','Serial Number') VALUES (?, ?, ?, ?);", to_db)

with open('2009_111_UCPA.csv','r') as fin: 
    dr = csv.DictReader(fin) 
    to_db = [(i['UCPA bureau 111'], i['Time'],i['Celsius(°C)'], i['Serial Number'] ) for i in dr]

cur.executemany("INSERT INTO UCPA_111 ('UCPA bureau 111','Time','Celsius(°C)','Serial Number') VALUES (?, ?, ?, ?);", to_db)

cur.execute("CREATE TABLE if not exists PP_213 ('Bur 213','Time','Celsius(°C)','Serial Number');") 

with open('2005_Bur_213.csv','r') as fin: 
    dr = csv.DictReader(fin) 
    to_db = [(i['Bur 213'], i['Time'],i['Celsius(°C)'], i['Serial Number'] ) for i in dr]

cur.executemany("INSERT INTO PP_213 ('Bur 213','Time','Celsius(°C)','Serial Number') VALUES (?, ?, ?, ?);", to_db)

with open('2006_Bur_213.csv','r') as fin: 
    dr = csv.DictReader(fin) 
    to_db = [(i['Bur 213'], i['Time'],i['Celsius(°C)'], i['Serial Number'] ) for i in dr]

cur.executemany("INSERT INTO PP_213 ('Bur 213','Time','Celsius(°C)','Serial Number') VALUES (?, ?, ?, ?);", to_db)

with open('2007_213_PP.csv','r') as fin: 
    dr = csv.DictReader(fin) 
    to_db = [(i['Bur 213'], i['Time'],i['Celsius(°C)'], i['Serial Number'] ) for i in dr]

cur.executemany("INSERT INTO PP_213 ('Bur 213','Time','Celsius(°C)','Serial Number') VALUES (?, ?, ?, ?);", to_db)

with open('2008_213_PP.csv','r') as fin: 
    dr = csv.DictReader(fin) 
    to_db = [(i['Bur 213'], i['Time'],i['Celsius(°C)'], i['Serial Number'] ) for i in dr]

cur.executemany("INSERT INTO PP_213 ('Bur 213','Time','Celsius(°C)','Serial Number') VALUES (?, ?, ?, ?);", to_db)

with open('2009_213_PP.csv','r') as fin: 
    dr = csv.DictReader(fin) 
    to_db = [(i['Bur 213'], i['Time'],i['Celsius(°C)'], i['Serial Number'] ) for i in dr]

cur.executemany("INSERT INTO PP_213 ('Bur 213','Time','Celsius(°C)','Serial Number') VALUES (?, ?, ?, ?);", to_db)

cur.execute("CREATE TABLE if not exists UCPA_101 ('UCPA 101','Time','Celsius(°C)','Serial Number');") 

with open('2006_UCPA_101.csv','r') as fin: 
    dr = csv.DictReader(fin) 
    to_db = [(i['UCPA 101'], i['Time'],i['Celsius(°C)'], i['Serial Number'] ) for i in dr]

cur.executemany("INSERT INTO UCPA_101 ('UCPA 101','Time','Celsius(°C)','Serial Number') VALUES (?, ?, ?, ?);", to_db)

with open('2007_101_UCPA.csv','r') as fin: 
    dr = csv.DictReader(fin) 
    to_db = [(i['UCPA 101'], i['Time'],i['Celsius(°C)'], i['Serial Number'] ) for i in dr]

cur.executemany("INSERT INTO UCPA_101 ('UCPA 101','Time','Celsius(°C)','Serial Number') VALUES (?, ?, ?, ?);", to_db)

with open('2008_101_UCPA.csv','r') as fin: 
    dr = csv.DictReader(fin) 
    to_db = [(i['UCPA 101'], i['Time'],i['Celsius(°C)'], i['Serial Number'] ) for i in dr]

cur.executemany("INSERT INTO UCPA_101 ('UCPA 101','Time','Celsius(°C)','Serial Number') VALUES (?, ?, ?, ?);", to_db)

with open('2009_101_UCPA.csv','r') as fin: 
    dr = csv.DictReader(fin) 
    to_db = [(i['UCPA 101'], i['Time'],i['Celsius(°C)'], i['Serial Number'] ) for i in dr]

cur.executemany("INSERT INTO UCPA_101 ('UCPA 101','Time','Celsius(°C)','Serial Number') VALUES (?, ?, ?, ?);", to_db)


con.commit()
con.close()




