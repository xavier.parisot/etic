import csv
import os
import sqlite3
import pandas as pd


path =  '/home/xavier/Bureau/projet_turbine/source'
  
obj = os.scandir()

for i in os.scandir(path):
    if i.is_file():
        print('File: ' + i.path)
    elif i.is_dir():
        print('Folder: ' + i.path) 
  

obj.close()

con = sqlite3.connect ('COOP.db')
cur = con.cursor()

location = r"/home/xavier/Bureau/projet_turbine/source/COOP_ENERC_MEZZ.txt"

df = pd.read_csv(location,encoding='iso-8859-1',header=0, quotechar='"')

df.to_csv('ENERC_MEZZ.csv',
               index = None)

df



cur.execute("CREATE TABLE if not exists coop_mez('COOP_ENERC_MEZZ','Time','Celsius(°C)','Serial Number');") 

with open('ENERC_MEZZ.csv','r') as fin: 
    dr = csv.DictReader(fin) 
    to_db = [(i['COOP_ENERC_MEZZ'], i['Time'],i['Celsius(°C)'], i['Serial Number'] ) for i in dr]

cur.executemany("INSERT INTO coop_mez ('COOP_ENERC_MEZZ','Time','Celsius(°C)','Serial Number') VALUES (?, ?, ?, ?);", to_db)


location = r"/home/xavier/Bureau/projet_turbine/source/COOP_CWK_R1.txt"

df1 = pd.read_csv(location,encoding='iso-8859-1',header=0, quotechar='"')

df1.to_csv('COOP_CWK_R1.csv',
               index = None)

df1


cur.execute("CREATE TABLE if not exists COOP_CWK_R1('COOP_CWK_R1','Time','Celsius(°C)','Serial Number');") 

with open('COOP_CWK_R1.csv','r') as fin: 
    dr = csv.DictReader(fin) 
    to_db = [(i['COOP_CWK_R1'], i['Time'],i['Celsius(°C)'], i['Serial Number'] ) for i in dr]

cur.executemany("INSERT INTO COOP_CWK_R1 ('COOP_CWK_R1','Time','Celsius(°C)','Serial Number') VALUES (?, ?, ?, ?);", to_db)


location = r"/home/xavier/Bureau/projet_turbine/source/COOPCuisine_R1.txt"

df2 = pd.read_csv(location,encoding='iso-8859-1',header=0, quotechar='"')

df2.to_csv('COOPCuisine_R1.csv',
               index = None)

df2




cur.execute("CREATE TABLE if not exists Cuisine_R1('COOPCuisine_R1','Time','Celsius(°C)','Serial Number');") 

with open('COOPCuisine_R1.csv','r') as fin: 
    dr = csv.DictReader(fin) 
    to_db = [(i['COOPCuisine_R1'], i['Time'],i['Celsius(°C)'], i['Serial Number'] ) for i in dr]

cur.executemany("INSERT INTO Cuisine_R1 ('COOPCuisine_R1','Time','Celsius(°C)','Serial Number') VALUES (?, ?, ?, ?);", to_db)


location = r"/home/xavier/Bureau/projet_turbine/source/COOP_ENERC_URSC.txt"

df3 = pd.read_csv(location,encoding='iso-8859-1',header=0, quotechar='"')

df3.to_csv('COOP_ENERC_URSC.csv',
               index = None)

df3




cur.execute("CREATE TABLE if not exists COOP_ENERC_URSC('COOP_ENERC_URSC','Time','Celsius(°C)','Serial Number');") 

with open('COOP_ENERC_URSC.csv','r') as fin: 
    dr = csv.DictReader(fin) 
    to_db = [(i['COOP_ENERC_URSC'], i['Time'],i['Celsius(°C)'], i['Serial Number'] ) for i in dr]

cur.executemany("INSERT INTO COOP_ENERC_URSC ('COOP_ENERC_URSC','Time','Celsius(°C)','Serial Number') VALUES (?, ?, ?, ?);", to_db)

location = r"/home/xavier/Bureau/projet_turbine/source/COOP_ENER_R2.txt"

df4 = pd.read_csv(location,encoding='iso-8859-1',header=0, quotechar='"')

df4.to_csv('COOP_ENER_R2.csv',
               index = None)

df4




cur.execute("CREATE TABLE if not exists COOP_ENER_R2('COOP_ENER_R2','Time','Celsius(°C)','Serial Number');") 

with open('COOP_ENER_R2.csv','r') as fin: 
    dr = csv.DictReader(fin) 
    to_db = [(i['COOP_ENER_R2'], i['Time'],i['Celsius(°C)'], i['Serial Number'] ) for i in dr]

cur.executemany("INSERT INTO COOP_ENER_R2 ('COOP_ENER_R2','Time','Celsius(°C)','Serial Number') VALUES (?, ?, ?, ?);", to_db)

location = r"/home/xavier/Bureau/projet_turbine/source/COOPPénisheR1su.txt"

df5 = pd.read_csv(location,encoding='iso-8859-1',header=0, quotechar='"')

df5.to_csv('COOPPénisheR1su.csv',
               index = None)

df5




cur.execute("CREATE TABLE if not exists COOPPénisheR1su('COOPPénisheR1su','Time','Celsius(°C)','Serial Number');") 

with open('COOPPénisheR1su.csv','r') as fin: 
    dr = csv.DictReader(fin) 
    to_db = [(i['COOPPénisheR1su'], i['Time'],i['Celsius(°C)'], i['Serial Number'] ) for i in dr]

cur.executemany("INSERT INTO COOPPénisheR1su ('COOPPénisheR1su','Time','Celsius(°C)','Serial Number') VALUES (?, ?, ?, ?);", to_db)

con.commit()
con.close()